# ts-spaceships

aicacia spaceships demo

## up and running

install [direnv](https://direnv.net/)

```bash
$ cp .envrc.example .envrc
```

start the app in dev

```bash
$ npm i
```

```bash
$ npm run gulp start
```

build the app in dev

```bash
$ npm run gulp build
```

## Docker/Kubernetes

build the docker image

```bash
$ npm run gulp docker.build
```
import "ts-polyfill";

import { events, sendMediaStream } from "../../libs/ts-cast-canvas-sender/lib";
import { createGame, OffscreenCtxRenderer } from "../lib";

async function onLoad() {
  const { canvas, offscreenCanvas, scene, loop } = await createGame();

  document.body.appendChild(canvas.getElement());

  function onStart() {
    scene.getPlugin(OffscreenCtxRenderer).ifSome(offscreenCtxRenderer => {
      offscreenCtxRenderer.setEnabled();
    });
    const mediaStream = (offscreenCanvas.getElement() as any).captureStream(24);
    sendMediaStream(mediaStream);
  }

  function onEnd() {
    scene.getPlugin(OffscreenCtxRenderer).ifSome(offscreenCtxRenderer => {
      offscreenCtxRenderer.setEnabled(false);
    });
  }

  function onResize(width: number, height: number) {
    offscreenCanvas.set(width, height);
  }

  events.on("start", onStart);
  events.on("end", onEnd);
  events.on("resize", onResize);

  loop.start();

  (window as any).scene = scene;
}

window.addEventListener("load", onLoad);

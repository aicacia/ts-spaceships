import { vec2 } from "gl-matrix";
import {
  angleVec2,
  Component,
  Entity,
  Input,
  Sprite,
  Transform2D,
  vec2FromAngle
} from "../../../libs/ts-engine/lib";
import { bulletAsset } from "../assets/assets";
import { Network } from "../plugins/Network";
import { Bullet } from "./Bullet";

const VEC2_0 = vec2.create();

export class Player extends Component {
  static componentName = "spaceships.Player";

  speed: number = 0.01;
  velocity: vec2 = vec2.create();

  onUpdate() {
    const input = this.getRequiredPlugin(Input),
      keys = vec2.set(
        VEC2_0,
        input.getRequiredAxisValue("horizontal-keys"),
        input.getRequiredAxisValue("vertical-keys")
      ),
      isFiring = input.isDown("Space"),
      transform = this.getRequiredComponent(Transform2D);

    vec2.scale(keys, keys, this.speed);
    vec2.add(this.velocity, this.velocity, keys);
    transform.translate(this.velocity);
    transform.setLocalRotation(angleVec2(this.velocity));

    vec2.scale(this.velocity, this.velocity, 0.95);

    if (isFiring) {
      const bulletDirection = vec2FromAngle(VEC2_0, transform.getRotation());
      this.getRequiredScene().addEntity(
        new Entity()
          .addTag("bullet")
          .addComponent(
            new Transform2D().setLocalPosition(transform.getPosition()),
            new Bullet(bulletDirection),
            new Sprite(bulletAsset).setWidth(0.3).setHeight(0.3)
          )
      );
      this.getRequiredPlugin(Network)
        .getClient()
        .broadcastFrom({
          type: "bullet",
          position: transform.getPosition(),
          direction: bulletDirection
        })
        .catch(this.onError);
    }

    this.getRequiredPlugin(Network)
      .getClient()
      .broadcastFrom({
        type: "player",
        position: transform.getPosition(),
        rotation: transform.getRotation()
      })
      .catch(this.onError);

    return this;
  }

  onError = (error: Error) => {};
}

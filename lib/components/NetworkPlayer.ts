import { RunOnUpdateComponent, Transform2D } from "../../../libs/ts-engine/lib";
import { EventType } from "../../../libs/ts-p2p/lib";
import { Network } from "../plugins/Network";

export class NetworkPlayer extends RunOnUpdateComponent {
  static componentName = "spaceships.NetworkPlayer";

  onAdd() {
    this.getRequiredPlugin(Network)
      .getClient()
      .on(EventType.MESSAGE, this.onMessage);
    return this;
  }
  onRemove() {
    this.getRequiredPlugin(Network)
      .getClient()
      .off(EventType.MESSAGE, this.onMessage);
    return this;
  }

  onMessage = (peerId: string, payload: any) => {
    this.getEntity().ifSome(entity => {
      if (entity.getName() === peerId && payload.type === "player") {
        this.enqueue(() => {
          const transform = this.getRequiredComponent(Transform2D);
          transform.setLocalPosition(payload.position);
          transform.setLocalRotation(payload.rotation);
        });
      }
    });
  };
}

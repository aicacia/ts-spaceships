import { vec2 } from "gl-matrix";
import { Component, Time, Transform2D } from "../../../libs/ts-engine/lib";

const VEC2_0 = vec2.create();

export class Bullet extends Component {
  static componentName = "spaceships.Bullet";

  speed: number = 20.0;
  spawnTime: number = 0.0;
  lifeTime: number = 1.0;
  direction: vec2 = vec2.create();

  constructor(direction: vec2) {
    super();
    vec2.copy(this.direction, direction);
  }

  onAdd() {
    this.spawnTime = this.getRequiredPlugin(Time).now();
    return this;
  }

  onUpdate() {
    if (this.spawnTime + this.lifeTime <= this.getRequiredPlugin(Time).now()) {
      this.getRequiredEntity().removeFromScene();
    } else {
      const transform = this.getRequiredComponent(Transform2D),
        velocity = vec2.scale(
          VEC2_0,
          this.direction,
          this.speed * this.getRequiredPlugin(Time).getDelta()
        );

      transform.translate(velocity);
    }
    return this;
  }
}

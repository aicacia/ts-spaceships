import { vec2 } from "gl-matrix";
import { Component, Transform2D } from "../../../libs/ts-engine/lib";

export class PlayerCamera extends Component {
  static componentName = "spaceships.PlayerCamera";

  private target: vec2 = vec2.create();

  onUpdate() {
    this.getRequiredScene()
      .findWithName("you")
      .flatMap(player => player.getComponent(Transform2D))
      .ifSome(transform2d => {
        vec2.copy(this.target, transform2d.getPosition());
      });

    this.getRequiredComponent(Transform2D).setLocalPosition(this.target);
    return this;
  }
}

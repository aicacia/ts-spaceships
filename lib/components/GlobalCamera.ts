import { vec2 } from "gl-matrix";
import { Camera2D, Component, Transform2D } from "../../../libs/ts-engine/lib";

const VEC2_0 = vec2.create();

export class GlobalCamera extends Component {
  static componentName = "spaceships.GlobalCamera";

  private min: vec2 = vec2.create();
  private max: vec2 = vec2.create();

  onAfterUpdate() {
    vec2.set(this.min, Infinity, Infinity);
    vec2.set(this.max, -Infinity, -Infinity);

    this.getRequiredScene()
      .findAllWithTag("player")
      .forEach(player => {
        const position = player.getRequiredComponent(Transform2D).getPosition();
        vec2.min(this.min, this.min, position);
        vec2.max(this.max, this.max, position);
      });

    const center = vec2.set(
        VEC2_0,
        this.min[0] + (this.max[0] - this.min[0]) * 0.5,
        this.min[1] + (this.max[1] - this.min[1]) * 0.5
      ),
      sizeX = (this.max[0] - this.min[0]) * 0.6,
      sizeY = (this.max[1] - this.min[1]) * 0.6;

    this.getRequiredComponent(Transform2D).setLocalPosition(center);
    this.getRequiredComponent(Camera2D).setSize(Math.max(sizeX, sizeY, 1));

    return this;
  }
}

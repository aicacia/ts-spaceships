import {
  Entity,
  Plugin,
  Sprite,
  Transform2D
} from "../../../libs/ts-engine/lib";
import { Client, EventType } from "../../../libs/ts-p2p/lib";
import { bulletAsset } from "../assets/assets";
import { Bullet } from "../components/Bullet";

export class Network extends Plugin {
  static pluginName = "spaceships.Network";

  private client: Client;

  constructor(client: Client) {
    super();
    this.client = client;
  }

  getClient() {
    return this.client;
  }

  onAdd() {
    this.getClient().on(EventType.MESSAGE, this.onMessage);
    return this;
  }
  onRemove() {
    this.getClient().off(EventType.MESSAGE, this.onMessage);
    return this;
  }

  onMessage = (peerId: string, payload: any) => {
    if (payload.type === "bullet") {
      this.getRequiredScene().addEntity(
        new Entity()
          .addTag("bullet")
          .addComponent(
            new Transform2D().setLocalPosition(payload.position),
            new Bullet(payload.direction),
            new Sprite(bulletAsset).setWidth(0.3).setHeight(0.3)
          )
      );
    }
  };
}

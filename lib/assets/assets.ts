import { ImageAsset } from "../../../libs/ts-engine/lib";
import bulletPng from "./bullet.png";
import spaceshipPng from "./spaceship.png";

export const spaceshipAsset = new ImageAsset(spaceshipPng);
export const bulletAsset = new ImageAsset(bulletPng);

import { vec3 } from "gl-matrix";
import {
  Assets,
  Camera2D,
  Canvas,
  CtxRenderer,
  CtxSpriteRendererHandler,
  Entity,
  FullScreenCanvas,
  Input,
  Loop,
  Scene,
  Sprite,
  Time,
  Transform2D
} from "../../libs/ts-engine/lib";
import { Client, EventType } from "../../libs/ts-p2p/lib";
import { bulletAsset, spaceshipAsset } from "./assets/assets";
import { GlobalCamera } from "./components/GlobalCamera";
import { NetworkPlayer } from "./components/NetworkPlayer";
import { Player } from "./components/Player";
import { PlayerCamera } from "./components/PlayerCamera";
import { Network } from "./plugins/Network";

export class OffscreenCtxRenderer extends CtxRenderer {
  static pluginName = "spaceships.OffscreenCtxRenderer";
}

export function createGame() {
  const client = new Client({
      url: `${window.env.APP_WS_URL}/socket`
    }),
    canvas = new Canvas(),
    offscreenCanvas = new Canvas(),
    input = new Input(canvas.getElement()),
    mainCamera = new Entity()
      .setName("main-camera")
      .addComponent(
        new Transform2D(),
        new PlayerCamera(),
        new Camera2D().setSize(10).setBackground(vec3.fromValues(0.1, 0.1, 0.1))
      ),
    offscreenCamera = new Entity()
      .setName("offscreen-camera")
      .addComponent(
        new Transform2D(),
        new GlobalCamera(),
        new Camera2D().setSize(10).setBackground(vec3.fromValues(0.1, 0.1, 0.1))
      ),
    player = new Entity()
      .setName("you")
      .addTag("player")
      .addComponent(
        new Transform2D(),
        new Player(),
        new Sprite(spaceshipAsset)
      ),
    scene = new Scene()
      .addEntity(mainCamera, offscreenCamera, player)
      .addPlugin(
        new Time(),
        new Network(client),
        input,
        new CtxRenderer(canvas).addRendererHandler(
          new CtxSpriteRendererHandler()
        ),
        new OffscreenCtxRenderer(offscreenCanvas)
          .addRendererHandler(new CtxSpriteRendererHandler())
          .setCamera(offscreenCamera.getComponent(Camera2D).unwrap())
          .setEnabled(false),
        new FullScreenCanvas(canvas),
        new Assets().addAsset(spaceshipAsset, bulletAsset).loadAllInBackground()
      ),
    loop = new Loop(() => {
      scene.update();
    });

  client.on(EventType.PEER_JOIN, peerId => {
    scene.addEntity(
      new Entity()
        .setName(peerId)
        .addTag("player")
        .addComponent(
          new Transform2D(),
          new NetworkPlayer(),
          new Sprite(spaceshipAsset)
        )
    );
  });
  client.on(EventType.PEER_LEAVE, peerId => {
    scene.findWithName(peerId).ifSome(entity => {
      scene.removeEntity(entity);
    });
  });

  return client.connect().then(() =>
    client.joinRoom("spaceships").then(() => {
      return { client, canvas, offscreenCanvas, loop, scene };
    })
  );
}
